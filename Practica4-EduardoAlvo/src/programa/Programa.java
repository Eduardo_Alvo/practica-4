package programa;

import java.util.Scanner;

import clases.Arsenal;

public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("**************************************");
		System.out.println("* Bienvenida a WEPS4ALL incorporated *");
		System.out.println("* ---------------------------------- *");
		System.out.println("*  A continuaci�n crear� su arsenal  *");
		System.out.println("*                                    *");
		System.out.println("* 1- Crear un arsenal desde cero     *");
		System.out.println("* 2- Crear un arsenal de tama�o 6    *");
		System.out.println("*    con 4 armas ya introducidas     *");
		System.out.println("*                                    *");
		System.out.println("**************************************");
		int opcion1 = input.nextInt();
		int tamanoArray = -1;
		boolean arrayPrefabricado = false;
		int opcion2;
		
		if (opcion1 == 1) {
			System.out.println();
			System.out.println("**************************************");
			System.out.println("*  Introduce el tama�o del arsenal   *");
			System.out.println("**************************************");
			tamanoArray = input.nextInt();
		}
		else {
			tamanoArray = 6;
			arrayPrefabricado = true;
		}
		
		
		
		Arsenal miArsenal = new Arsenal(tamanoArray);
		if(arrayPrefabricado) {
			miArsenal.altaArma("001", "Ak-47", "7.62", 4.3, "Fusil de asalto", 443);
			miArsenal.altaArma("002", "M1911", "11.43", 1.13, "Pistola", 50);
			miArsenal.altaArma("003", "Kar 98K", "7.92", 4.1, "Fusil", 500);
			miArsenal.altaArma("004", "AR-15", "5.56", 2.97, "Fusil de asalto", 457);
			
			System.out.println("Las siguientes armas han sido creadas: ");
			miArsenal.listarArmas();
		}
		System.out.println();
		System.out.println("**************************************");
		System.out.println("*    Arsenal creado correctamente    *");
		System.out.println("**************************************");
		
		do {
			System.out.println();
			System.out.println("**************************************");
			System.out.println("*              MENU                  *");
			System.out.println("* 1- Dar de alta un arma             *");
			System.out.println("* 2- Buscar un arma por su codigo    *");
			System.out.println("* 3- Eliminar un arma por su codigo  *");
			System.out.println("* 4- Listar todas las armas          *");
			System.out.println("* 5- Cambiar el nombre de un arma    *");
			System.out.println("* 6- Cambiar el tipo de un arma      *");
			System.out.println("* 7- Listar armas por su tipo        *");
			System.out.println("* 8- Salir del programa              *");
			System.out.println("**************************************");
			
			opcion2 = input.nextInt();
			
			switch(opcion2) {
			
			case 1:
				input.nextLine();
				System.out.println();
				System.out.println("**************************************");
				System.out.println("*   Introduce el codigo del arma     *");
				System.out.println("**************************************");
				String codigoArma = input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el nombre del arma     *");
				System.out.println("**************************************");
				String nombreArma = input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el calibre del arma    *");
				System.out.println("**************************************");
				String calibreArma = input.nextLine();
				System.out.println("**************************************");
				System.out.println("*    Introduce el peso del arma      *");
				System.out.println("**************************************");
				double pesoArma = input.nextDouble();
				input.nextLine();
				System.out.println("**************************************");
				System.out.println("*    Introduce el tipo del arma      *");
				System.out.println("**************************************");
				String tipoArma = input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el alcance del arma    *");
				System.out.println("**************************************");
				double alcanceArma = input.nextDouble();
				
				
				miArsenal.altaArma(codigoArma, nombreArma, calibreArma, pesoArma, tipoArma, alcanceArma);
				
				break;
				
			case 2: 
				input.nextLine();
				System.out.println();
				System.out.println("**************************************");
				System.out.println("*   Introduce el codigo del arma     *");
				System.out.println("**************************************");
				String codigoArma1 = input.nextLine();
				
				System.out.println(miArsenal.buscarArma(codigoArma1).toString());
				
				break;
				
			case 3:
				input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el codigo del arma     *");
				System.out.println("**************************************");
				String codigoArma2 = input.nextLine();
				miArsenal.eliminarArma(codigoArma2);
				System.out.println("**************************************");
				System.out.println("*   Arma eliminada correctamente     *");
				System.out.println("**************************************");
				break;
			
			case 4: 
				miArsenal.listarArmas();
				
				break;
				
			case 5:
				input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el codigo del arma     *");
				System.out.println("**************************************");
				String codigoArma3 = input.nextLine();
				
				System.out.println("**************************************");
				System.out.println("* Introduce el nuevo nombre del arma *");
				System.out.println("**************************************");
				String nombreArma1 = input.nextLine();
				
				miArsenal.cambiarNombreArma(codigoArma3, nombreArma1);

				
				break;
				
			case 6:
				input.nextLine();
				System.out.println("**************************************");
				System.out.println("*   Introduce el codigo del arma     *");
				System.out.println("**************************************");
				String codigoArma4 = input.nextLine();
				
				System.out.println("**************************************");
				System.out.println("*  Introduce el nuevo tipo del arma  *");
				System.out.println("**************************************");
				String tipoArma1 = input.nextLine();
				
				miArsenal.cambiarTipoArma(codigoArma4, tipoArma1);
				
				break;
				
			case 7:
				input.nextLine();
				System.out.println("**************************************");
				System.out.println("*    Introduce el tipo del arma      *");
				System.out.println("**************************************");
				String tipoArma2 = input.nextLine();
				
				miArsenal.listarArmaPorTipo(tipoArma2);
				
				break;
				
			case 8:
				System.out.println("**************************************");
				System.out.println("*      Saliendo del programa         *");
				System.out.println("**************************************");
				
				break;
				
			default:
				System.out.println("**************************************");
				System.out.println("*   Introduce una opcion correcta    *");
				System.out.println("**************************************");
				
			
			}
			
			
		}while(opcion2 != 8);
		
		
		input.close();
		
		
		
		

	}

}
