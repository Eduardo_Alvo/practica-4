package clases;

public class Arsenal {
	private Arma[] armas;
	
	public Arsenal(int maximoArmas) {
		armas = new Arma[maximoArmas];
	}
	
	
	public void altaArma (String codArma, String nombre, String calibre, double peso, String tipo, double alcance) {
		for (int i = 0; i < armas.length; i++) {
			if (armas[i] == null) {
				armas[i] = new Arma(codArma);
				armas[i].setNombre(nombre);
				armas[i].setCalibre(calibre);
				armas[i].setPeso(peso);
				armas[i].setTipo(tipo);
				armas[i].setAlcance(alcance);
				break;
			}
		}
	}
	
	public Arma buscarArma(String codArma) {
		for (int i = 0; i < armas.length; i++) {
			if (armas[i] != null) {
				if (armas[i].getCodArma().equals(codArma)) {
					return armas[i];
				}
			}
		}
		return null;
	}
	
	public void eliminarArma(String codArma) {
		for (int i = 0; i < armas.length; i++) {
			if(armas[i] != null) {
				if (armas[i].getCodArma().equals(codArma)) {
					armas[i] = null;
				}
			}
		}
	}
	
	public void listarArmas() {
		for (int i = 0; i < armas.length; i++) {
			if (armas[i] != null) {
				System.out.println(armas[i].toString());
			}
		}
	}
	
	public void cambiarTipoArma(String codArma, String tipo) {
		for (int i = 0; i < armas.length; i++) {
			if (armas[i] != null) {
				if (armas[i].getCodArma().equals(codArma)) {
					armas[i].setTipo(tipo);
				}
			}
		}
	}
	
	public void cambiarNombreArma(String codArma, String nombre) {
		for (int i = 0; i < armas.length; i++) {
			if(armas[i] != null) {
				if (armas[i].getCodArma().equals(codArma)) {
					armas[i].setNombre(nombre);
				}
			}
		}
	}
	
	public void listarArmaPorTipo(String tipo) {
		for (int i = 0; i < armas.length; i++) {
			if (armas[i] != null) {
				if (armas[i].getTipo().equals(tipo)) {
					System.out.println(armas[i].toString());
				}
			}
		}
	}
	
	
	
	
	
	
	
}
