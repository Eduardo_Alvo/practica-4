package clases;

public class Arma {
	
	private String codArma;
	private String nombre;
	private String calibre;
	private double peso;
	private String tipo;
	private double alcance;
	
	public Arma(String codArma) {
		this.codArma = codArma;
	}
	
	
	public String getCodArma() {
		return codArma;
	}
	public void setCodArma(String codArma) {
		this.codArma = codArma;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCalibre() {
		return calibre;
	}
	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public double getAlcance() {
		return alcance;
	}
	public void setAlcance(double alcance) {
		this.alcance = alcance;
	}
	
	public String toString() {
		return "Codigo: " + codArma + ", nombre: " + nombre + ", calibre: " + calibre + ", peso: " +
				peso + ", tipo: " + tipo + ", alcance efectivo: " + alcance;
	}
	
	
	
}
